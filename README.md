# README #

This README

### What is this repository for? ###

* Quick start html/css/js project

### How do I get set up? ###

* Download this repo to your localhost
* Open command terminal and make sure to change directory to you project: cd /dir/
* Install the latest version of npm: npm install -g npm@latest
* Install bower: npm install -g bower
* Install gulp: npm install -g gulp-cli
* Install npm dependencies: npm install
* Install bower dependencies: bower install
* Start gulp: gulp
* Gulp will create directory /build/ and launch you default brouser (http://localhost:3000/) with Project
* Edit all files only in /src/ directory, gulp will copy files to /build/ automaticaly

### Who do I talk to? ###

* Roman